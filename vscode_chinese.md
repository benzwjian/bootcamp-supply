## 設定 VScode 介面為中文

#### 步驟一
按下 `Ctrl + Shite + P`，在輸入框打 `Lang`，然後選擇 `Configure Display Language` 選項。

![images/vscode_01.png](images/vscode_01.png)

#### 步驟二
選擇 `Install additional languages...` 選項。

![images/vscode_02.png](images/vscode_02.png)

#### 步驟三
在側邊欄的中文繁體項目，按下 `install`。

![images/vscode_03.png](images/vscode_03.png)

#### 步驟四
完成安裝後，在右下角提示視窗，按下 `Yes`，重新啟動 VScode。

![images/vscode_04.png](images/vscode_04.png)

#### 步驟五
完成中文化!

![images/vscode_05.png](images/vscode_05.png)