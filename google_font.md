## 使用更好看的字型
這裡示範如何使用 Google Font 的字型

#### 步驟一
先到 [Google Font](https://fonts.google.com/) 網站，然後點選你想要的字型。

![google_font_01](images/google_font_01.png)

#### 步驟二
決定字型的粗細類型，然後點選 `Select the style`，這時候會出現右邊側邊欄。

![google_font_02](images/google_font_02.png)

#### 步驟三
在側邊欄選擇 `Embed` 的分頁，我們要複製底下兩段程式碼。

![google_font_03](images/google_font_03.png)

#### 步驟四
那兩段程式碼
* 一段是 `<link>` 標籤，直接放在 `<head>` 標籤內
* 一段是 CSS 樣式規則，放在 `<style>` 標籤內，並指定好標籤選擇器

```html
<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@100&display=swap" rel="stylesheet">
        <style>
            p {
                font-family: 'Noto Sans TC', sans-serif;
            }
        </style>
    </head>
    <body>
        <p>測試文字</p>
    </body>
</html>
```
